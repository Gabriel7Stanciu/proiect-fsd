import axios from "axios";
import dotenv from 'dotenv';

dotenv.config();

const axiosInstance = () => {
  const options = {
    baseURL: "http://localhost:3001",
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Create instance
  let instance = axios.create(options);

  // Set the AUTH token for any request
  instance.interceptors.request.use(function (config: any) {
    const token = localStorage.getItem('token');
    config.headers.Authorization = token ? token : '';
    return config;
  });

  return instance;
};

export default axiosInstance();