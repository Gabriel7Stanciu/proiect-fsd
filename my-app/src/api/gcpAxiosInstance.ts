import axios from "axios";

const gcpAxiosInstance = () => {
    const options = {
      baseURL: "https://us-central1-sturdy-ranger-334316.cloudfunctions.net",
      headers: {
        'Content-Type': 'application/json',
      },
    };
  
    // Create instance
    let instance = axios.create(options);
  
    return instance;
  };

  export default gcpAxiosInstance();
