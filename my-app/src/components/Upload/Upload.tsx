import { Button, Card, CardMedia, Paper } from '@mui/material';
import { Box } from '@mui/system';
import axiosInstance from "../../api/instance";
import { useRef, useState } from 'react';
import useLocalStorage from 'use-local-storage';

export default function Upload() {

  const pageStyle = {
    background: '#CCD1D1',
    height: '100vh',
    width: '100vw',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minHeight: "100vh"
  }

  const [imageSrc, setImageSrc] = useState(
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQE3CETL_OertJKScoHfblxs6CBrKGVCmVESw&usqp=CAU"
  );

  //localStorage.removeItem("savedImages")

  const [tableImages, setTableImages] = useLocalStorage("savedImages", JSON.stringify([]));


  const inputFile = useRef<HTMLInputElement | null>(null);

  const onButtonClick = () => {
    if (inputFile.current != null) {
      inputFile.current.click();
    }
  };

  function saveResultToLocalStorage(file: File, prediction: Number, src: string) {
    const previousImages = JSON.parse(tableImages);

    const result = {
      id: previousImages.length,
      imageName: file.name,
      size: file.size,
      recognitionResult: prediction,
      imageDownloadLink: src,
    };

    setTableImages(JSON.stringify([...previousImages, result]));
  }

  const onInputChange = (e: any) => {
    if (e.target.files != null) {

      var formData = new FormData();

      if (e.target.files != null) {
        formData.append("sampleFile", e.target.files[0]);

        try {
          axiosInstance.post("evaluate", formData, {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }).then((response) => {

            var reader = new FileReader();

            reader.onloadend = function () {
              
              if (reader.result != null) {
                setImageSrc(reader.result.toString());
                saveResultToLocalStorage(e.target.files[0], response.data["prediction"], reader.result.toString());
              }
            };

            reader.readAsDataURL(e.target.files[0]);

          }
          );

        } catch (error: any) {
          console.log(error);
        }

      }

    }
  };


  const paperStyle = {
    justifyContent: "center",
    alignItems: "center",
    height: "70vh", display: 'flex',
    padding: 0, background: '#8FA6A6', marginTop: '4%'
  }

  const btnStyle = { background: '#8FA6A6', marginLeft: '68%', marginTop: '2%', color: 'black' }

  return (

    <Box style={pageStyle}>
      <div style={{ height: '85vh', width: '56vw', backgroundColor: 'transparent' }}>

        <Button
          type='submit'
          style={btnStyle}
          onClick={onButtonClick}
        >
          Upload
        </Button>

        <input
          type="file"
          id="file"
          ref={inputFile}
          onChange={onInputChange}
          style={{ display: "none" }}
        />

        <Card style={paperStyle}>
          <CardMedia style={{ textAlign: "center" }}>
            <img src={imageSrc} />
          </CardMedia>
        </Card>

      </div>
    </Box >
  )
}