import { DataGrid, GridColDef, GridRowsProp } from '@mui/x-data-grid';
import { Box } from '@mui/system';
import useLocalStorage from 'use-local-storage';
import { useEffect, useState } from 'react';

const pageStyle = {
  background: '#CCD1D1',
  height: '100vh',
  width: '100vw',
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  minHeight: "100vh"
}

const columns: GridColDef[] = [
  { field: 'imageName', headerName: 'Image name', headerAlign: "left", width: 160 },
  { field: 'size', headerName: 'Size', headerAlign: "right", width: 110, align: 'right' },
  { field: 'recognitionResult', headerName: 'Recognition result', headerAlign: "right", width: 200, align: 'right' },
  { field: 'imageDownloadLink', headerName: 'Image download link', headerAlign: "right", width: 210, align: 'right' }]

export default function Table() {

  const [savedImages] = useLocalStorage("savedImages", JSON.stringify([]));
  const [rows, setRows] = useState<Array<Object>>([]);

  useEffect(() => { setRows(JSON.parse(savedImages)); }, []);


  return (
    <Box style={pageStyle}>
      <div style={{ height: '85vh', width: '58vw', backgroundColor: 'white' }}>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={8}
          rowsPerPageOptions={[8]}
          checkboxSelection
        />
      </div>
    </Box>
  );
}