import { Divider, Stack } from "@mui/material";
import { DatePickerField } from "./DatePickerField";

function StartAndEndDates () {
    return (
        <div style={{ padding: "10px", alignItems: "center" }} >
            <Stack
                direction="row"
                justifyContent="space-evenly"
                width="40vw"
                height="60px"
                alignItems="center"
                divider={<Divider orientation="vertical" flexItem />}
                style={{
                    width: 600,
                    height: 80,
                    padding: "5px",
                    border: "1px solid LightGrey",
                    borderRadius: "12px",
                }}>
                <DatePickerField label={"Start date"} date={Date()} color={"#947be4"} />
                <DatePickerField label={"End date"} date={Date()} color={"#72a5e7"} />
            </Stack>
        </div>
    );
}

export default StartAndEndDates;