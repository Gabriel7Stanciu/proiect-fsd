import { useState } from 'react';
import './datePicker.scss'

const range = (start: number, end: number) => Array.from({ length: (end - start) }, (v, k) => k + start);

function Calendar() {
    const weekDays = Array<string>( "SU", "MO", "TU", "WE", "TH", "FR", "SA").map((day) =>
        <li>{day}</li>);

    return (
        <div className="grid-container">
            <div className="table-header">
                <div className="grid-item">
                    <span className="current-day">
                        15
                    </span>
                    <br />
                    TODAY
                </div>
                <div className="grid-item vertical-line">AUGUST &#9660;</div>
                <div className="grid-item vertical-line">2018 &#9660;</div>
            </div>

            <div className="calendar">
                <ul className="weekdays">
                    {weekDays}
                </ul>

                <ul className="days">
                    <span id="previous-month-days">
                        {range(29, 32).map((value) => (
                            <li>{value}</li>
                        ))}
                    </span>
                    <span id="current-month-days">
                        {range(1, 15).map((value) => (
                            <li>{value}</li>
                        ))}
                        <li><span className="active">15</span></li>
                        {range(16, 27).map((value) => (
                            <li>{value}</li>
                        ))}
                        <span className="interval">
                            {range(27, 32).map((value) => (
                                <li>{value}</li>
                            ))}
                        </span>
                    </span>
                    <span id="previous-month-days">
                    {range(1, 9).map((value) => (
                            <li>{value}</li>
                        ))}
                    </span>

                </ul>

            </div>

            <div className="buttons-container">
                <button className="cancelbtn">CANCEL</button>
                <button className="confirmbtn">DONE</button>
            </div>
        </div >
    );
}

export default Calendar;