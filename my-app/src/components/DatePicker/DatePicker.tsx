import * as React from 'react';
import TextField from '@mui/material/TextField';
import DateRangePicker, { DateRange } from '@mui/lab/DateRangePicker';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import Box from '@mui/material/Box';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import { Button, Divider, Grid, Paper, Snackbar, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@mui/material';
import './datePicker.scss'
import moment from 'moment';
import gcpAxiosInstance from '../../api/gcpAxiosInstance';


interface Schedule {
  id: number;
  start_date: Date | null;
  end_date: Date | null;
}

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});


export default function DatePicker() {

  const [disable, setDisable] = React.useState(false);
  const [value, setValue] = React.useState<DateRange<Date>>([null, null]);
  const [rows, setRows] = React.useState<Array<Schedule>>([]);
  const [error, setError] = React.useState("");
  const [nextId, setNextId] = React.useState(0);

  React.useEffect(() => {
    gcpAxiosInstance.get('getSchedules')
      .then((response) => {
        let array = response.data.map((elem: any, i: Number) => ({ id: i, start_date: elem.start_date, end_date: elem.end_date } as Schedule));
        setRows(array);
        setNextId(array.length);
      }
      );
  }, []);


  const onSave = () => {
    setDisable(true);
    setTimeout(() => setDisable(false), 150);

    let newSchedule = { id: nextId, start_date: value[0], end_date: value[1] };
    setNextId(nextId + 1);
    setRows([...rows, newSchedule]);
    setValue([null, null]);

    gcpAxiosInstance.post('createSchedule',
      {
        start_date: value[0],
        end_date: value[1],
      }
    ).catch((error) => {

      setError(error.response.data["error"]);
      setOpen(true);

      setRows(rows.filter((schedule) => schedule.id !== newSchedule.id));
      setDisable(false);
    });
  };

  const [open, setOpen] = React.useState(false);

  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  return (
    <Grid container spacing={0}
      direction="column"
      alignItems="center"
      justifyContent="center">

      <header className="header">
        Date Picker
      </header>

      <Stack
        direction="row"
        justifyContent="space-evenly"
        width="40vw"
        height="60px"
        alignItems="center"
        divider={<Divider orientation="vertical" flexItem />}
        style={{
          width: 600,
          height: 80,
          padding: "5px",
          border: "1px solid LightGrey",
          borderRadius: "12px",
        }}>

        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DateRangePicker
            startText="Start date"
            endText="End date"
            calendars={1}
            value={value}
            inputFormat="dd-MM-yyyy"
            onChange={(newValue) => {
              setValue(newValue);
            }}
            renderInput={(startProps, endProps) => (
              <React.Fragment>
                <TextField {...startProps} />
                <Box sx={{ mx: 2 }}>
                  <Divider />
                </Box>
                <TextField {...endProps} />
              </React.Fragment>
            )}
          />

        </LocalizationProvider>

        <Button variant="contained" onClick={onSave} disabled = {disable}>Save</Button>

      </Stack>

      <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
          {error}
        </Alert>
      </Snackbar>


      <TableContainer elevation={8} component={Paper} style={{ height: '65vh', width: '20vw', backgroundColor: 'white', marginTop: 30 }}>
        <Table stickyHeader aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={{ fontWeight: 600, fontSize: 16 }}>Start date</TableCell>
              <TableCell style={{ fontWeight: 600, fontSize: 16 }}>End date</TableCell>
            </TableRow>
          </TableHead>
          <TableBody style={{ overflow: "auto" }}>
            {rows.map((row, index) => (
              <TableRow key={index.toString()}>
                <TableCell>
                  {moment(row.start_date).format('DD-MM-YYYY')}
                </TableCell>
                <TableCell>
                  {moment(row.end_date).format('DD-MM-YYYY')}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

    </Grid>

  );
}
