import { Stack } from '@mui/material';
import moment from 'moment';
import { useContext } from 'react';
import CalendarContext from '../../Contexts';

type DatePickerFieldProps = {
    label: string;
    date: string;
    color: string;
};

export const DatePickerField: React.FC<DatePickerFieldProps> = (
    props: DatePickerFieldProps
) => {
    const {showCalendar, setShowCalendar} = useContext(CalendarContext);
    return (
        <div onClick={(e) => setShowCalendar(!showCalendar)} style={{ width: "100vw" }}>
            <Stack spacing={0} direction="column" alignItems="center">
                <div className="label">
                    {props.label}
                </div>
                <div className="selected-date" style={{ color: props.color }}>
                    {moment(props.date).format("DD MMM YYYY").toUpperCase()}
                </div>
            </Stack>
        </div>
    );
};