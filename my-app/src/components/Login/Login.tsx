import { Avatar, Button, Container, Grid, Link, TextField, Typography } from '@mui/material';
import { Box } from '@mui/system';
import { InputAdornment } from "@mui/material";
import EmailIcon from '@mui/icons-material/Email';
import LockIcon from '@mui/icons-material/Lock';
import { useRef, useState } from 'react'
import AvatarImg from '../../resources/avatar_img.png'

import { UserContext } from '../../Contexts'
import styled from '@emotion/styled';
import useAuth from '../../hooks/useAuth';

export default function Login() {
  const pageStyle = {
    background: 'linear-gradient(to right top, blue, magenta)',
    height: '100vh',
    width: '100vw',
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minHeight: "100vh"
  }

  const CustomTextField = styled(TextField)(() => ({
    '& fieldset': {
      borderRadius: '25px'
    },
  })); 

  const btnstyle = { margin: '20px 0', borderRadius: 35, height: 70, backgroundColor: '#1bbd61' }

  const [email, setEmail] = useState("aaa");
  const [password, setPassword] = useState("aaa");

  return (

    <Box style={pageStyle}>
      <Container sx={{ m: 20, p: 15, borderRadius: 5, background: 'White' }}>
        <Grid container spacing={10}>
          <Grid item xs={7} alignItems='center' flexDirection='column'>
            <Avatar src={AvatarImg} sx={{ width: 350, height: 350, ml: 10, mt: 4, alignItems: 'center' }} />
          </Grid>

          <Grid item xs={5}>
            <Box component="form" sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}>
              <Typography sx={{ fontSize: 45, mb: 4, mr: 5, fontFamily: 'Century Gothic', fontWeight: 'bold', color: '#2e2e2e' }}> User Login
              </Typography>
            </Box>

            <Box component="form" sx={{
              mt: 1, mr: 5, display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}>

              <CustomTextField
                style={{ backgroundColor: 'rgba(220,200,220,0.2)', borderRadius: 25 }}
                fullWidth
                id="email"
                type="email"
                placeholder="Email"
                margin="normal"
                // onChange={(e)=>{
                //   setEmail(e.target.value); 
                // }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <EmailIcon />
                    </InputAdornment>
                  )
                }}
                InputLabelProps={{ style: { fontSize: 40 } }}
              />
              <CustomTextField
                style={{ backgroundColor: 'rgba(220,200,220,0.2)', borderRadius: 25 }}
                fullWidth
                id="password"
                type="password"
                placeholder="Password"
                margin="normal"
                // onChange={(e)=>setPassword(e.target.value)}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <LockIcon />
                    </InputAdornment>
                  )
                }}
              />
              <Button sx={{ fontSize: 20, fontFamily: 'Century Gothic', fontWeight: 'bold', color: "white" }} type='submit'
                color='primary' variant="contained" style={btnstyle} fullWidth
                onClick={(e) => {
                  //e.preventDefault();
                  useAuth.login(email, password);
                 // console.log(localStorage.getItem("token"));
                }}>
                Login
              </Button>
              <Typography>
                <Link href="#" sx={{ fontSize: 20, fontFamily: 'Century Gothic', fontWeight: 'bold', color: "#575757" }} style={{ textDecoration: 'none' }}>
                  Forgot Username / Password ?
                </Link>
              </Typography>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}