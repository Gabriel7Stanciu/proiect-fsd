import { useMemo, useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Redirect } from "react-router";

import Login from './components/Login/Login';
import Upload from './components/Upload/Upload';
import Table from './components/Table/Table';
import DatePicker from './components/DatePicker/DatePicker';
import CalendarContext, { UserContext } from './Contexts';

require('dotenv').config();

function App() {  
  const [logged, setLogged] = useState(true);

  return (

    <BrowserRouter>
      <Switch>
        
        <UserContext.Provider value={{ logged, setLogged }}>
          <Route exact path={"/"}>
            <Login />
          </Route>
       
          <Route exact path={"/table"}>
            {logged ? <Table /> : <Redirect to="/" />}
          </Route>
          <Route exact path={"/upload"}>
            {logged ? <Upload /> : <Redirect to="/" />}
          </Route>

          <Route exact path={"/calendar"}>
            <DatePicker />
          </Route>

        </UserContext.Provider>
      
      </Switch>
    </BrowserRouter>
  );
}

export default App;