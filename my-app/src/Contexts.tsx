import React from 'react';

export const UserContext = React.createContext({
    logged: false,
    setLogged: (value: boolean) => { }
});

const CalendarContext = React.createContext({
    showCalendar: false,
    setShowCalendar: (show: boolean) => {}
  });
  
  export default CalendarContext;