import axiosInstance from "../api/instance";

require('dotenv').config();

const auth = {
  async login(email: string, password: string): Promise<any> {
    try {
      const response = axiosInstance.post("/login", {
        email,
        password,
      }).then((response) => {
          if (response.data.token) {
            localStorage.setItem("token", response.data.token);
          }
        });
    } catch (error: any) {
      console.log(error);
    }

  },
  logout() {
    localStorage.removeItem("token");
  }
}

export default auth